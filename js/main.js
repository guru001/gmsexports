Vue.component('sp',{
    template:`
    <span class="of-blue" >
    <slot></slot>
    </span>
    `,
});

Vue.component('icon',{
    props:{
     type:{type:String,default:''},
     ico:{type:String,default:''},
    }
    ,
    template:`
    <span :class=" 'icon '+type ">
  <i :class=" 'fa fa-'+ico"></i>
</span>
    `
});
let vue = new Vue({
    el:"#main",
  
     methods:{
         setJson(data)
         {
     this.json = data;
     console.log('set json successful.');
         },
       cardamon()
       {
         this.modal_cardamon=true ^ this.modal_cardamon;
  
         
       },
       

     },
     mounted(){
       

        }   ,
    data:{
heading:'GMS Exports and Traders',
modal_cardamon:false,
json:'',
cardamon_table:[
    { col1:"Cardamom Size Available",col2:"6.5mm to 7mm"},
    { col1:"Packaging Type Available",col2:	"Gunny Bag, Plastic Bag, PP Bag"},
    {  col1:"Packaging Size Available(In Kg/In Ton)",col2:"	5 Kg, 10 Kg, 50 Kg "},
    { col1:"Minimum Order Quantity",col2:	"500 Kilogram"}
],
cardamon_grades:[	"AGEB(Alleppey Green Extra Bold )","AGB(Alleppey Green Bold)","	AGS(Alleppey Green Superior)",
    "	AGS-1(Alleppey Green Shipment Green-1)", "	AGS-2(Alleppey Green Shipment Green-2)"]

    },
    computed:{
        is_cardamon(){
          return (this.modal_cardamon)?'is-active':'';
        }
    }
    ,
    components:{
        'Collapse':httpVueLoader('../views/collapse.vue'),
        'Hero':httpVueLoader('../views/Hero.vue'),
        'h-Form':httpVueLoader('../views/form.vue'),
        'modal':httpVueLoader('../views/modal.vue')
    }
});